from telegram import Update
from telegram.ext import ApplicationBuilder

from app.internal.transport.bot.handlers import HANDLERS


def start_polling(telegram_api_url: str, telegram_token: str) -> None:
    app = ApplicationBuilder().token(telegram_token).base_url(telegram_api_url).build()
    app.add_handlers(HANDLERS)

    app.run_polling(allowed_updates=Update.ALL_TYPES)
    app.start()
