from typing import Optional

import telegram

from app.internal.models import User


async def get_user(*, tg_id: Optional[int] = None, username: Optional[str] = None) -> Optional[User]:
    try:
        if tg_id:
            return await User.objects.aget(tg_id=tg_id)
        elif username:
            return await User.objects.aget(username=username)
        else:
            raise ValueError("cannot get user without id or username")
    except User.DoesNotExist:
        return None


async def get_or_create_user(*, tg_id: int, username: str, first_name: Optional[str], last_name: Optional[str]) -> User:
    if user := await get_user(tg_id=tg_id):
        return user

    return await User.objects.acreate(
        tg_id=tg_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
    )


async def set_user_phone_number(user: User, phone_number: str) -> None:
    user.phone_number = phone_number
    await user.asave(update_fields=("phone_number",))


async def get_from_telegram_user(tg_user: Optional[telegram.User]) -> Optional[User]:
    if not tg_user:
        raise ValueError("Empty telegram user was provided")
    return await get_user(tg_id=tg_user.id)


async def get_or_create_from_telegram_user(tg_user: Optional[telegram.User]) -> User:
    if not tg_user:
        raise ValueError("Empty telegram user was provided")
    return await get_or_create_user(
        tg_id=tg_user.id,
        username=tg_user.username,
        first_name=tg_user.first_name,
        last_name=tg_user.last_name,
    )
