import re

PHONE_REGEX_STR = r"^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$"
PHONE_REGEX = re.compile(PHONE_REGEX_STR)


def normalize_phone_number(number: str) -> str:
    return number.strip().replace("-", "").replace(" ", "")


def is_valid_phone_number(number: str) -> bool:
    naked_number = normalize_phone_number(number)
    return PHONE_REGEX.fullmatch(naked_number) is not None
