from django.contrib import admin

from app.internal.models import User


@admin.register(User)
class UserAdmin:
    list_display = ("tg_id", "username", "first_name", "last_name", "phone_number")
