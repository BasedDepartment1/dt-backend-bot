from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.models import User
from app.internal.services import user_service, validator
from app.internal.transport.bot.decorators import with_authorized_user


async def start(update: Update, _: CallbackContext) -> None:
    user = await user_service.get_or_create_from_telegram_user(update.effective_user)

    await update.message.reply_text(f"Hello, {user.username}")


@with_authorized_user(need_phone_number=False)
async def set_phone(user: User, update: Update, ctx: CallbackContext) -> None:
    if len(ctx.args) != 1:
        await update.message.reply_text(
            f"You have passed invalid number of args: {len(ctx.args)}. Please use this format:\n"
            "/set_phone PHONE_NUMBER",
        )
        return

    phone_number = validator.normalize_phone_number(ctx.args[0])
    if not validator.is_valid_phone_number(phone_number):
        await update.message.reply_text("You have passed invalid phone number.")
        return

    await user_service.set_user_phone_number(user, phone_number)
    await update.message.reply_text("Great! Now your phone numer is set.")


ME_MESSAGE_TEMPLATE = """
Here is all your information that I know:

Id: `{user_id}`
Username: `{username}`
First Name: `{first_name}`
Last Name: `{last_name}`
Phone Number: `{phone_number}`
""".strip()


@with_authorized_user(need_phone_number=True)
async def me(user: User, update: Update, _: CallbackContext) -> None:
    await update.message.reply_markdown(
        ME_MESSAGE_TEMPLATE.format(
            user_id=user.tg_id,
            username=user.username,
            first_name=user.first_name,
            last_name=user.last_name,
            phone_number=user.phone_number,
        )
    )


HANDLERS = [
    CommandHandler("start", start),
    CommandHandler("set_phone", set_phone),
    CommandHandler("me", me),
]
