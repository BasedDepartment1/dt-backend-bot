import functools
from typing import Callable, Coroutine

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models import User
from app.internal.services import user_service

TelegramHandler = Callable[[Update, CallbackContext], Coroutine]
AuthorizedTelegramHandler = Callable[[User, Update, CallbackContext], Coroutine]


def with_authorized_user(*, need_phone_number: bool) -> Callable[[AuthorizedTelegramHandler], TelegramHandler]:
    def decorator(fn: AuthorizedTelegramHandler) -> TelegramHandler:
        @functools.wraps(fn)
        async def wrapper(upd: Update, ctx: CallbackContext) -> None:
            user = await user_service.get_from_telegram_user(upd.effective_user)
            if user is None:
                await upd.message.reply_markdown(
                    "Somehow you have not been authorized :(\n"
                    "Before setting your phone please run command ```/start```"
                )
                return

            if need_phone_number and not user.phone_number:
                await upd.message.reply_markdown(
                    "You did not provided your phone number.\n"
                    "Please set it before running this command using ```/set_phone```"
                )
                return

            return await fn(user, upd, ctx)

        return wrapper

    return decorator
