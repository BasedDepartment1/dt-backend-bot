from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from app.internal.services.user_service import get_user


class MeHandler(View):
    async def get(self, _: HttpRequest, user_id: int) -> HttpResponse:
        user = await get_user(tg_id=user_id)
        if user is None:
            return HttpResponse(content=f"No user with id {id} found", status=404)

        return JsonResponse(user.to_dict())
