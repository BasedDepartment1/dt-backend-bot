from django.urls import path

from app.internal.transport.rest.handlers import MeHandler

urlpatterns = [path("me/<int:user_id>", MeHandler.as_view(), name="me")]
