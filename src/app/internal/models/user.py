from django.core.validators import RegexValidator
from django.db import models

from app.internal.services.validator import PHONE_REGEX_STR


class User(models.Model):
    tg_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=255, unique=True, db_index=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    _phone_validator = RegexValidator(regex=PHONE_REGEX_STR)
    phone_number = models.CharField(max_length=63, validators=(_phone_validator,))

    def __str__(self) -> str:
        return str(self.username)

    def to_dict(self):
        return {
            "tg_id": self.tg_id,
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": self.phone_number,
        }
