from django.core.management.base import BaseCommand

import app.internal.bot
from config.settings import env


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        app.internal.bot.start_polling(
            env("TELEGRAM_API_URL"),
            env("TELEGRAM_TOKEN"),
        )
