# Репозиторий курса по бэкенду

## Требования
- Python 3.10
- PostgreSQL 16.1

## Установка зависимостей
```shell
pipenv install
```

## Запуск бота
```shell
make runbot
```

## Запуск сервера
```shell
make dev  # или make command runserver localhost:port
```